import java.util.*;
import java.io.*;
import static java.util.stream.Collectors.*;
import java.lang.*;
import java.util.stream.*;
import java.util.stream.Collectors;

class VariousOperations {

    public void findByPinCode(List<Class> clsList, List<Address> address, List<Student> student, int pinCode) {
        for (Address a1: address) {
            if (a1.getPin_code() == pinCode) {
                for (Student s1: student) {
                    if (a1.student_id == s1.getId()) {
                        System.out.println(s1.getName());
                    }
                }
            }
        }
    }

    public void findByCity(List<Class> clsList, List<Address> address, List<Student> student, String city) {
        for (Address a1: address) {
            if (a1.getCity().equals(city)) {
                for (Student s1: student) {
                    if (a1.student_id == s1.getId()) {
                        System.out.println(s1.getName());
                    }
                }
            }
        }
    }

    public void uploadClass(List<Class> classes) {
        String line = "";
        String splitBy = ",";

        try {
            BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\Admin\\IdeaProjects\\JavaTrainingDayTwo\\CSV Files\\Class.csv"));

            try {
                line = br.readLine();

                while (line != null) {
                    String[] temp = line.split(splitBy);
                    // System.out.println(Integer.parseInt(temp[0]));
                    classes.add(new Class(Integer.parseInt(temp[0]), temp[1]));
                    line = br.readLine();
                }
            }
            catch (NumberFormatException ne) {
                System.out.println(ne);
            }
        }
        catch (IOException e) {
            System.out.println(e);
        }
        catch (Exception e) {
            System.out.println(e);
        }

        System.out.println("\nFile Class Read Successfully");
        System.out.println("Reading Details from object of \"class.csv\" file: ");
        for (Class c1: classes) {
            System.out.print("Name: " + c1.getName() + ", ");
            System.out.println("Id: " + c1.getId());
            // System.out.println("Class: " + c1.getClass());
        }
    }

    public void uploadAddress(List<Address> address) {
        String line = "";
        String splitBy = ",";

        try {
            BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\Admin\\IdeaProjects\\JavaTrainingDayTwo\\CSV Files\\Address.csv"));

            try {
                line = br.readLine();

                while (line != null) {
                    String[] temp = line.split(splitBy);
                    // System.out.println(Integer.parseInt(temp[0]));

                    address.add(new Address(Integer.parseInt(temp[0]), Integer.parseInt(temp[1]), temp[2], Integer.parseInt(temp[3])));
                    line = br.readLine();
                }
            }
            catch (NumberFormatException ne) {
                System.out.println(ne);
            }
        }
        catch (IOException e) {
            System.out.println(e);
        }

        System.out.println("\nFile Address Read Successfully");
        System.out.println("Reading Details from object of \"Address.csv\" file: ");
        for (Address a1: address) {
            System.out.print("Id = " + a1.getId() + ", ");
            System.out.print("Pin code = " + a1.getPin_code() + ", ");
            System.out.print("City = " + a1.getCity() + ", ");
            System.out.println("Student ID = " + a1.getStudent_id());

//             System.out.println("Class: " + a1.getClass());
        }
    }

    public void uploadStudent(List<Student> student) {
        String line = "";
        String splitBy = ",";

        try {
            BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\Admin\\IdeaProjects\\JavaTrainingDayTwo\\CSV Files\\Student.csv"));

            try {
                line = br.readLine();

                while (line != null) {
                    String[] temp = line.split(splitBy);
                    // System.out.println(Integer.parseInt(temp[0]));

                    if (Integer.parseInt(temp[5]) <= 20) {
                        student.add(new Student(Integer.parseInt(temp[0]), temp[1], temp[2], Integer.parseInt(temp[3]), temp[4], Integer.parseInt(temp[5])));
                    }
                    line = br.readLine();
                }
            }
            catch (NumberFormatException ne) {
                System.out.println(ne);
            }
        }
        catch (IOException e) {
            System.out.println(e);
        }

        System.out.println("\nFile Student Read Successfully");
        System.out.println("Reading Details from object of \"Student.csv\" file: ");
        for (Student s1: student) {
            System.out.print("Id = " + s1.getId() + ", ");
            System.out.print("Name = " + s1.getName() + ", ");
            System.out.print("Class = " + s1.getClass_id() + ", ");
            System.out.print("Marks = " + s1.getMarks() + ", ");
            System.out.print("Gender = " + s1.getGender() + ", ");
            System.out.print("Age = " + s1.getAge() + ", ");


            System.out.println("Class: " + s1.getClass());
        }
    }

    public void pagination(List<Student> student, int pageNo, int pageSize, String gender, String orderBy) {
        System.out.println("\nStudent Names ordered by " + orderBy + " having gender = " + gender);
        if (orderBy.toLowerCase().equals("name")) {
            HashMap<Integer, String> hm = new HashMap<Integer, String>();
            for (int i = (pageNo - 1) * pageSize; i < pageNo * pageSize; i++) {
                hm.put(student.get(i).getId(), student.get(i).getName());
            }

            List<Map.Entry<Integer, String>> hm1 = new ArrayList<>(hm.entrySet());

            Collections.sort(hm1, new Comparator<Map.Entry<Integer, String>>() {
                @Override
                public int compare(Map.Entry<Integer, String> o1, Map.Entry<Integer, String> o2) {
                    return o1.getValue().compareTo(o2.getValue());
                }
            });

            // System.out.println("\nStudent Names ordered by name having gender = " + gender);
            for (int i = 0; i < hm1.size(); i++) {
                System.out.println(hm1.get(i).getValue() + " ");
            }
        }
        else if (orderBy.toLowerCase().equals("marks")) {
            HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
            for (int i = (pageNo - 1) * pageSize; i < pageNo * pageSize; i++) {
                hm.put(student.get(i).getId(), student.get(i).getMarks());
            }

            List<Map.Entry<Integer, Integer>> hm1 = new ArrayList<>(hm.entrySet());

            Collections.sort(hm1, new Comparator<Map.Entry<Integer, Integer>>() {
                @Override
                public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                    return o2.getValue().compareTo(o1.getValue());
                }
            });

            // System.out.println("\nStudent Names ordered by marks having gender = " + gender);
            for (int i = 0; i < hm1.size(); i++) {
                // System.out.println(hm1.get(i).getKey());
                for (int j = 0; j < student.size(); j++) {
                    if (hm1.get(i).getKey() == student.get(j).getId()) {
                        System.out.println(student.get(j).getName() + " " + student.get(j).getMarks());
                    }
                }
            }
        }
    }
}

public class TaskOnStudents {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        VariousOperations obj = new VariousOperations();
        /*
        List<Class> classes = new ArrayList<Class>();



         */

        List<Class> clsList = new ArrayList<Class>();

        // I should be able to upload 3 files to fill the collection of these three Objects.

        obj.uploadClass(clsList);

        /*
        clsList.add(new Class(1,"A"));
        clsList.add(new Class(2,"B"));
        clsList.add(new Class(3,"C"));
        clsList.add(new Class(4,"D"));

         */

        List<Address> address = new ArrayList<Address>();

        obj.uploadAddress(address);

        /*
        address.add(new Address(1,452002,"indore",1));
        address.add(new Address(2,422002,"delhi",1));
        address.add(new Address(3,442002,"indore",2));
        address.add(new Address(4,462002,"delhi",3));
        address.add(new Address(5,472002,"indore",4));
        address.add(new Address(6,452002,"indore",5));
        address.add(new Address(7,432002,"delhi",5));
        address.add(new Address(8,482002,"mumbai",6));
        address.add(new Address(9,492002,"bhopal",7));
        address.add(new Address(10,482002,"indore",8));
        */

        List<Student> student = new ArrayList<Student>();

        obj.uploadStudent(student);

        /*
        student.add(new Student(1,"stud1","A",88,"F",10));
        student.add(new Student(2,"stud2","A",70,"F",11));
        student.add(new Student(3,"stud3","B",88,"M",22));
        student.add(new Student(4,"stud4","B",55,"M",33));
        student.add(new Student(5,"stud5","A",30,"F",44));
        student.add(new Student(6,"stud6","C",30,"F",33));
        student.add(new Student(7,"stud6","C",10,"F",22));
        student.add(new Student(8,"stud6","C",0,"M",11));
        */


        String tempGender, tempClass, tempCity;
        int tempAge, tempPinCode;
        char ch;
        int pinCode;
        String city;


        // Find all students of pin code X(ex X = 482002). I can pass different filters like gender, age, class
        System.out.println("\nFind Students by Pin Code:");

        System.out.println("\nEnter Pin code: ");
        pinCode = 482002;
        // pinCode = scan.nextInt();

        /*
        System.out.print("Enter \"Y\" to add gender filter else enter \"N\": ");
        ch = scan.next().charAt(0);

        if (ch == 'Y' || ch == 'y') {
            System.out.println("Enter gender (M or F)");
        }
        else {
            tempGender = "";
        }
        */
        obj.findByPinCode(clsList, address, student, pinCode);




        // Find all students of city ex X = Indore. I can pass different filters like gender, age, class
        System.out.println("\nFind students by city: ");

        System.out.println("\nEnter city: ");
        city = "indore";
        // city = scan.next();
        obj.findByCity(clsList, address, student, city);




        /*
        marks < 50 failed else passed
        Give ranks to highest mark achievers.
        Highest marks - First
        Second Highest marks - Second
        Third Highest marks - third
        Rest of all pass / fail
        */

        System.out.println("\nIf marks < 50 then failed else passed\n");

        Map<String, Integer> studentMarks = new HashMap<>();
        for (Student s1: student) {
            studentMarks.put(s1.getName(), s1.getMarks());
        }

        List<Map.Entry<String, Integer>> sortedStudentMarks = new ArrayList<>(studentMarks.entrySet());

        Collections.sort(sortedStudentMarks, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        System.out.println("Rank    Name    Marks   Pass/Fail");
        for (int i = 0; i < sortedStudentMarks.size(); i++) {
            System.out.print(i + 1 + "\t\t");
            System.out.print(sortedStudentMarks.get(i).getKey() + "\t");
            System.out.print(sortedStudentMarks.get(i).getValue() + "\t\t");
            if (sortedStudentMarks.get(i).getValue() >= 50) {
                System.out.println("Passed");
            }
            else {
                System.out.println("Failed");
            }
        }

        // Get the passed students. I can pass different filters like gender, age, class, city, pin code
        System.out.println("\nGetting the passed students: ");

        for (Student s1: student) {
            if (s1.getMarks() >= 50) {
                System.out.println(s1.getName());
            }
        }


        // Get the failed Students
        System.out.println("\nGetting the failed students: ");

        for (Student s1: student) {
            if (s1.getMarks() < 50) {
                System.out.println(s1.getName());
            }
        }

        // Find all student of class X (ex X = A).  I can pass different filters like gender, age, class, city, pin code
        System.out.println("Enter any class (A, B, C or D): ");
        tempClass = "A";
        // tempClass = scan.next();

        System.out.println("\nAll Students of Class " + tempClass + " are: ");
        for (Student s1: student) {
            if (s1.getClass_id().equals(tempClass)) {
                System.out.println(s1.getName());
            }
        }



        // I should be able to delete student. After that it should delete the respective obj from Address & Student.
        System.out.println("\nDelete Student:-");

        System.out.println("\nBefore Deletion:");
        for (int i = 0; i < student.size(); i++) {
            System.out.println(i + "\t" + student.get(i).getId() + "\t" + student.get(i).getName() + ", ");
        }

        // Deleting student at specified index
        try {
            System.out.print("Enter index of student to be deleted: ");
            int deleteIndex = 3;
            // deleteIndex = scan.nextInt();
            if (student.size() == 1) {
                student.clear();
                System.out.println("Last element deleted.\nStudent Class empty.");
            }
            else {
                student.remove(deleteIndex);
                System.out.println("Student at index " + deleteIndex + " deleted successfully.");
            }

            /*
            System.out.println("\nAfter Deletion:");
            for (int i = 0; i < student.size(); i++) {
                System.out.println(i + "\t" + student.get(i).getId() + "\t" + student.get(i).getName() + ", ");
            }
            */
        }
        catch (Exception e) {
            System.out.println(e);
        }



        // I should be able to read paginated students.
        System.out.println("\nI should be able to read paginated students.");
        String orderBy;


        try {
            System.out.println();
            orderBy = "name";
            obj.pagination(student, 2, 5, "F", orderBy);

            orderBy = "name";
            obj.pagination(student, 2, 5, "M", orderBy);

            orderBy = "marks";
            obj.pagination(student, 2, 5, "F", orderBy);

            orderBy = "marks";
            obj.pagination(student, 2, 5, "M", orderBy);
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }
}
